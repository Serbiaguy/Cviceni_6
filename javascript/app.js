/*jslint es6 */

var main_controller = null;
var container = [];

class Controller {
    constructor(users, tasks) {
        this.users = users;
        this.tasks = tasks;
    }

    findTask(id) {
        for(var i = 0 ; i < this.tasks.length; i++) {
            if(id == this.tasks[i].id) {
                return this.tasks[i];
            }
        }

        return null;
    }

    getUser(user) {
        for(var i = 0 ; i < this.users.length; i++) {
            if(this.users[i].email === user) {
                return this.users[i];
            }
        }

        return null;
    }

    getUsers() {
        return this.users;
    }

    findAllTasks(user, done) {
        var usr = this.getUser(user);
        var tasks = [];

        if(usr === null) {
            return null;
        }

        for(var i = 0; i < usr.groups.length; i++) {
            for(var j = 0; j < usr.groups[i].tasks.length; j++) {
                if(done === undefined) {
                    tasks.push(usr.groups[i].tasks[j]);
                }else if(usr.groups[i].tasks[j].done == done){
                    tasks.push(usr.groups[i].tasks[j]);
                }
            }
        }

        return tasks;
    }

    getGroup(group) {

    }

    checkTitle(group, title) {
        for(var i = 0; i < group.tasks.length; i++) {
            if(group.tasks[i].title === title) {
                return false;
            }
        }

        return true;
    }

    getLastTaskId(group) {
        for(var i = 0; i < group.tasks.length; i++) {
            if(i == group.tasks.length - 1) {
                return group.tasks[i].id + 2;
            }
        }

        return -1;
    }

    addTask(group, title, due_date) {
        for(var i = 0; i < this.users.length; i++ ) {
            for(var j = 0; j < this.users[i].groups.length; j++) {
                if(this.users[i].groups[j].id === group) {
                    if(this.checkTitle(this.users[i].groups[j], title)) {
                        var task = new Task(this.getLastTaskId(this.users[i].groups[j]), title, null, this.users[i].groups[j], "12-12-2018");
                        this.users[i].groups[j].addTask(task);
                        this.tasks.push(task);
                    }
                }
            }
        }
    }

    removeLocalTask(id) {
        for(var j = 0; j < this.tasks.length; j++) {
            if(this.tasks[j].id == id && this.tasks[j].done) {
                this.tasks.splice(j, 1);
            }
        }
    }

    removeTask(id) {
        for(var i = 0; i < this.users.length; i++) {
            for(var j = 0; j < this.users[i].groups.length; j++) {
                for(var k = 0; k < this.users[i].groups[j].tasks.length; k++) {
                    if(this.users[i].groups[j].tasks[k].id == id && this.users[i].groups[j].tasks[k].done) {
                        this.users[i].groups[j].tasks.splice(k, 1);
                        this.removeLocalTask(id);
                    }
                }
            }
        }
    }

    markAsDone(id) {
        for(var i = 0; i < this.users.length; i++) {
            for(var j = 0; j < this.users[i].groups.length; j++) {
                for(var k = 0; k < this.users[i].groups[j].tasks.length; k++) {
                    if(this.users[i].groups[j].tasks[k].id == id) {
                        this.users[i].groups[j].tasks[k].markAsDone();
                        return;
                    }
                }
            }
        }
    }

    markAsOpen(id) {
        for(var i = 0; i < this.users.length; i++) {
            for(var j = 0; j < this.users[i].groups.length; j++) {
                for(var k = 0; k < this.users[i].groups[j].tasks.length; k++) {
                    if(this.users[i].groups[j].tasks[k].id == id) {
                        this.users[i].groups[j].tasks[k].markAsOpen();
                        return;
                    }
                }
            }
        }
    }
}

class Task {
    constructor(id,
                title,
                description,
                done,
                group,
                due_date) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.done = done;
        this.group = group || null;
        this.creted = new Date().getDay() + "-" + new Date().getMonth() + "-" + new Date().getFullYear();
        this.due_date = due_date;
    }

    toString() {
        return this.description;
    }

    addGroup(group) {
        this.group = group;
    }

    markAsDone() {
        this.done = true;
    }

    markAsOpen() {
        this.done = false;
    }
}

class Group {
    constructor(id, title, owner, tasks) {
        this.id = id;
        this.title = title;
        this.owner = owner || null;
        this.tasks = tasks || [];
    }

    setOwner(owner) {
        this.owner = owner;
    }

    addTask(task) {
        this.tasks.push(task);
    }
}

class User {
    constructor(email, groups) {
        this.email = email;
        this.groups = groups || [];
    }

    addGroups(groups) {
        this.groups.push(groups)
    }
}



function initController() {
    console.log("Initation");
    var task_1 = new Task(1, "cisteni oken", "Umyt",false,null,  "2-4-2018");
    var task_2 = new Task(2, "uklid kancelari", "Uklidit kancelarske prostory",false,null, "2-4-2018");
    var task_3 = new Task(3, "vytvoreni CRM", "Vytvorit CRM system",false,null, "1-5-2018");
    var task_4 = new Task(4, "vyplata mzdy", "Vyplatit mzdy zamestnancum za mesic brezen",false,null, "11-4-2018");


    var group_1 = new Group(1, "Uklizeci sluzba", null, [task_1, task_2]);
    var group_2 = new Group(2, "Programatori", null, [task_3]);
    var group_3 = new Group(3, "Ucetni", null, [task_4] );

    var user_1 = new User("head_master@gmail.com", [group_2, group_3]);
    var user_2 = new User("head_maid@gmail.com", [group_1]);

    group_1.setOwner(user_2);
    group_2.setOwner(user_1);
    group_3.setOwner(user_1);

    task_1.addGroup(group_1);
    task_2.addGroup(group_1);
    task_3.addGroup(group_2);
    task_4.addGroup(group_3);

    main_controller = new Controller([user_1, user_2], [task_1, task_2, task_3, task_4]);

    var sel = document.getElementById("users");
    var users = main_controller.getUsers();

    for(var i = 0; i < users.length;  i++) {

        var option = document.createElement("option");

        option.innerHTML = users[i].email;
        option.value = users[i].email;

        sel.append(option);

    }

}

function clearList() {
    var list = document.getElementById("list_of_tasks");
    console.log(container);
    for(var i = 0 ; i < container.length; i++) {
        console.log(document.getElementById(container[i] + '_li'));
        list.removeChild(document.getElementById(container[i] + '_li'));
    }

    container = [];
}


function handleChange(e) {

    if(e === undefined) {
        return;
    }

    var id = e.target.parentNode.id[0];

    if (e.target.checked) {
       main_controller.markAsDone(id);
       document.getElementById(id + '_la').style.textDecoration = "line-through";
       document.getElementById(id + '_la').style.color = "gray";
    }else {
        main_controller.markAsOpen(id);
        document.getElementById(id + '_la').style.textDecoration = "none";
        document.getElementById(id + '_la').style.color = "black";
    }
}

function handleClick() {

    var sel = document.getElementById("users");

    var tasks = main_controller.findAllTasks(sel.options[sel.selectedIndex].value, false);
    var list = document.getElementById("list_of_tasks");

    console.log(tasks);

    for (var i = 0; i < tasks.length; i++) {
        var list_item = document.createElement("li");
        list_item.appendChild(CreateCheckBox(tasks[i].description, tasks[i].id));
        list_item.id = tasks[i].id + "_li";
        container.push(tasks[i].id);
        list.appendChild(list_item);
    }
}


function CreateCheckBox(value, id, done) {
    var checkBox = document.createElement("input");
    var label = document.createElement("label")
    var description = document.createTextNode(value.toString());

    checkBox.type = "checkbox";
    checkBox.value = value.toString();
    checkBox.addEventListener("change", handleChange, false);

    label.appendChild(description);
    label.appendChild(checkBox);
    label.id = id + "_la";
    label.addEventListener("change", handleChange, false);

    done ? label.style.textDecoration = "line-through" : label.style.textDecoration = "none";
    done ? label.style.color = "gray" : label.style.color = "black";

    return label;
}

window.addEventListener('load', function() {
    initController();
})
